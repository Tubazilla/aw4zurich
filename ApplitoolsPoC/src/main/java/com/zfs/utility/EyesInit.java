package com.zfs.utility;

import com.applitools.eyes.BatchInfo;
import com.applitools.eyes.selenium.BrowserType;
import com.applitools.eyes.selenium.Configuration;
import com.applitools.eyes.selenium.Eyes;
import com.applitools.eyes.visualgrid.model.DeviceName;
import com.applitools.eyes.visualgrid.services.VisualGridRunner;

public class EyesInit {

    public static Eyes initializeEyes(String BatchName, String AppName, String TestName, VisualGridRunner runner) {
        // Create Eyes object with the runner, meaning it'll be a Visual Grid eyes.
        Eyes eyes = new Eyes(runner);

        // Set API key
        eyes.setApiKey(System.getenv("APPLITOOLS_API_KEY"));

        //If dedicated or on-prem cloud, uncomment and enter the cloud url
        //Default: https://eyes.applitools.com
        //eyes.setServerUrl("https://testeyes.applitools.com");

        // Create SeleniumConfiguration.
        Configuration sconf = new Configuration();

        // Set the AUT name
        sconf.setAppName(AppName);

        // Set a test name
        sconf.setTestName(TestName);

        // Set a batch name so all the different browser and mobile combinations are
        // part of the same batch
        sconf.setBatch(new BatchInfo(BatchName));

        // Add Chrome browsers with different Viewports
        sconf.addBrowser(1024, 768, BrowserType.CHROME);
        sconf.addBrowser(2048, 1536, BrowserType.CHROME);

        // Add Firefox browser with different Viewports
        sconf.addBrowser(1024, 768, BrowserType.FIREFOX);
        sconf.addBrowser(2048, 1536, BrowserType.FIREFOX);

        // Add IE browser with different Viewports
        sconf.addBrowser(1024, 768, BrowserType.IE_11);
        sconf.addBrowser(2048, 1536, BrowserType.IE_11);

        // Add Edge browser with different Viewports
        sconf.addBrowser(1024, 768, BrowserType.EDGE);
        sconf.addBrowser(2048, 1536, BrowserType.EDGE);

        sconf.addDeviceEmulation(DeviceName.iPhone_6_7_8);
        sconf.addDeviceEmulation(DeviceName.Pixel_2);

        // Set the configuration object to eyes
        eyes.setConfiguration(sconf);

        return eyes;
    }


}
