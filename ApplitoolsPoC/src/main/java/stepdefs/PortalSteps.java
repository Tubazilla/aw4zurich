package stepdefs;

import com.applitools.eyes.MatchLevel;
import com.applitools.eyes.RectangleSize;
import com.applitools.eyes.selenium.Eyes;
import com.applitools.eyes.visualgrid.model.TestResultSummary;
import com.applitools.eyes.visualgrid.services.VisualGridRunner;
import com.zfs.utility.EyesInit;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class PortalSteps {

	WebDriver driver;

	// Initialize the eyes SDK and set your private API key.
	// Create a runner with concurrency of 10
	VisualGridRunner runner = new VisualGridRunner(10);

	//Initialize Eyes with Visual Grid Runner
	String AppName = "ZFS-Portal-App";
	String TestName = "ZFS-Portal-App-Test";
	String BatchName = "ZFS-Portal-App-Test-Batch";
	Eyes eyes = EyesInit.initializeEyes(BatchName, AppName, TestName, runner);
	//Eyes eyes = new Eyes();
	String elementXPath;
	String elementCSSSelector;

	@And("^I Close the Browser")
	public void close_the_browser() throws Throwable {
		Thread.sleep(5000);
		try {
			//eyes.close();
			TestResultSummary allResults = runner.getAllTestResults();
		} catch (Exception e) {

			e.printStackTrace();

			// If the test was aborted before eyes.close was called, ends the test as aborted.
			eyes.abortIfNotClosed();

		} finally {

			// Close the browser.
			driver.quit();

		}

	}

	@Given("I navigate to the Zurich Portal")
	public void WhenINavigateToTheZurichPortal() throws Throwable {

		eyes.setApiKey(System.getenv("APPLITOOLS_API_KEY"));

		eyes.setMatchTimeout(10000);
		eyes.setMatchLevel(MatchLevel.STRICT);

		driver = new ChromeDriver();

		driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("https://zporttest.azurewebsites.net/login");

		//********************** E N T E R    P R O X Y   I N F O   H E R E ***************************
		//if you need to specify internet proxy info do it here
		//eyes.setProxy(new ProxySettings("http://YOUR-PROXY-URI"));
		//OR
		//eyes.setProxy(new ProxySettings("http://YOUR-PROXY-URI", YOUR_USER, YOUR_PASSWORD));
		//*********************************************************************************************


		eyes.open(driver, AppName, TestName,
				new RectangleSize(1024, 768));

	}

	@Then("I run lots of tests")
	public void ThenIRunLotsOfTests() throws Throwable {

		Wait<WebDriver> wait = new FluentWait<>(driver)
				.withTimeout(Duration.ofSeconds(30))
				.pollingEvery(Duration.ofMillis(200))
				.ignoring(NoSuchElementException.class);

			eyes.open(driver, AppName, TestName,
					new RectangleSize(1024, 768));

			//eyes.open(driver, "Zurich FS", "Portal Test1");

			eyes.setMatchTimeout(30000);
			eyes.setMatchLevel(MatchLevel.STRICT);

			//STEP 1
			//navigate to portal login and check window
			driver.get("https://zporttest.azurewebsites.net/login");
			Thread.sleep(5000);
			eyes.checkWindow();

			//STEP 2
			//log in and check window
			driver.findElement(By.id("username")).sendKeys("DemoTestUser");
			driver.findElement(By.id("password")).sendKeys("New@1234");
			driver.findElement(By.cssSelector(".btn")).click();

			// check portal homepage
			eyes.setMatchLevel(MatchLevel.LAYOUT2);
			Thread.sleep(10000);
			eyes.checkWindow();


			// make sure we are in the correct community to avoid login issues downstream
            /*
            elementXPath = "//html/body/app-root/div/div/app-page-header/header/div[3]";
            WebElement elCommunities = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXPath)));
            Thread.sleep(5000);
            elCommunities.click();

            elementCSSSelector = "body > modal-container > div > div > div.modal-body > div:nth-child(1) > div > ng-select";
            WebElement elCommunity = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(elementCSSSelector)));
            Thread.sleep(2000);
            elCommunity.click();

            WebElement el6 = driver.findElement(By.cssSelector("body > modal-container > div > div > div.modal-body > div:nth-child(1) > div > ng-select > div > div > div.ng-input > input[type=text]"));
            el6.sendKeys("abc (123)");

            driver.findElement(By.cssSelector("body > modal-container > div > div > div.modal-body > div:nth-child(1) > div > ng-select > div > span.ng-arrow-wrapper")).click();
            driver.findElement(By.cssSelector("body > modal-container > div > div > div.modal-footer > button")).click();
            */

			//STEP 3
			String notInspectedCountCheck = "13";
			String notInspectedCount = "";
			WebElement el1 = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/app-root/div/div/div/app-dashboard/app-widgets-grid/div/div/div[3]/app-graph-widget/app-base-widget/article/div[2]/div/div/app-donut-chart/div/div/div/strong")));

			notInspectedCount = el1.getText();

			if (!notInspectedCount.equals(notInspectedCountCheck)) {
				System.err.println("Not Inspected Count: " + notInspectedCount + " but " + notInspectedCountCheck + " was expected");
				System.err.println(" Test Continues...");
			}

			Thread.sleep(10000);
			elementCSSSelector = ".fa-chart-bar";
			WebElement el8 = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(elementCSSSelector)));
			el8.click();
			elementCSSSelector = ".goto-data-page";
			WebElement el9 = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(elementCSSSelector)));
			el9.click();
			elementXPath = "/html/body/app-root/div/div/div/app-data/app-widgets-grid/div/div/div[4]/app-graph-widget/app-base-widget/article/div[1]/app-date-selector/div/app-dropdown/div/button/span";
			WebElement el2 = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXPath)));

			Thread.sleep(10000);
			eyes.checkWindow();

			//STEP 4
			String OKOBScheck = "OK(Obs)";
			try {
				WebElement el7 = driver.findElement(By.xpath("/html/body/app-root/div/div/div/app-data/app-widgets-grid/div/div/div[4]/app-graph-widget/app-base-widget/article/div[2]/div/div/app-stacked-bars-chart/div/div/ul/li[1]/span[2]"));
				String OKOBS = el7.getText();
				if (!OKOBS.equals(OKOBScheck)) {
					System.err.println("OK(Obs) Legend is: " + OKOBS + " but " + OKOBScheck + " was expected");
					System.err.println(" Test Continues...");
				}
			} catch (Exception e) {
				//no issue - if no data found in portlet
			}
			el2.click();
			Thread.sleep(3000);
			WebElement el3 = driver.findElement(By.xpath("/html/body/app-root/div/div/div/app-data/app-widgets-grid/div/div/div[4]/app-graph-widget/app-base-widget/article"));
			eyes.checkElement(el3);


			//STEP 5
			driver.findElement(By.xpath("/html/body/app-root/div/div/div/app-data/app-widgets-grid/div/div/div[4]/app-graph-widget/app-base-widget/article/div[1]/app-date-selector/div/app-dropdown/div/ul/li[1]")).click();

			//STEP 6
			/*
			Thread.sleep(5000);
			eyes.checkElement(el3);

            //STEP 7
            driver.findElement(By.xpath("/html/body/app-root/div/div/app-page-header/header/div[4]/div[1]")).click();

            //STEP 8
            driver.findElement(By.xpath("/html/body/app-root/div/div/app-page-header/header/div[4]/div[2]/div")).click();
            driver.findElement(By.xpath("/html/body/app-root/div/div/app-page-header/header/div[4]/div[2]/div/div/a[1]")).click();

            //STEP 9
            WebElement el4 = driver.findElement(By.xpath("/html/body/app-root/div/div/div/app-admin/div/div[2]/div/div/div/app-user-management/div/div/input"));
            eyes.checkWindow();
            el4.click();

            //STEP 10
            //fill in form
            //name
            String nameSel = "body > app-root > div > div > div > app-user-preferences > div.c-container > div:nth-child(4) > div > div > div > form > div:nth-child(1) > input";
            Thread.sleep(500);
            driver.findElement(By.cssSelector(nameSel)).sendKeys("Applitools");

            //telephone
            String telSel = "body > app-root > div > div > div > app-user-preferences > div.c-container > div:nth-child(4) > div > div > div > form > div:nth-child(3) > input";
            Thread.sleep(500);
            driver.findElement(By.cssSelector(telSel)).sendKeys("7305082461");

            //password
            String pwSel = "body > app-root > div > div > div > app-user-preferences > div.c-container > div:nth-child(4) > div > div > div > form > div:nth-child(5) > div > input";
            Thread.sleep(500);
            driver.findElement(By.cssSelector(pwSel)).sendKeys("Customer@123");

            //email address
            String emailSel = "body > app-root > div > div > div > app-user-preferences > div.c-container > div:nth-child(4) > div > div > div > form > div:nth-child(2) > input";
            Thread.sleep(500);
            driver.findElement(By.cssSelector(emailSel)).sendKeys("mondal.namam89@gmail.com");

            //user role
            String urSelContainer = "body > app-root > div > div > div > app-user-preferences > div.c-container > div:nth-child(4) > div > div > div > form > div.form-group.right.col-md-6.ng-star-inserted > ng-select";
            String urSelRow = "body > app-root > div > div > div > app-user-preferences > div.c-container > div:nth-child(4) > div > div > div > form > div.form-group.right.col-md-6.ng-star-inserted > ng-select > div > div > div.ng-input > input[type=text]";
            Thread.sleep(500);
            driver.findElement(By.cssSelector(urSelContainer)).click();
            Thread.sleep(500);
            //driver.findElement(By.cssSelector(urSelRow)).sendKeys("Read All");
            driver.findElement(By.xpath("//span[text()='Read All']")).click();

            //multi-select ** BUTTON DOES NOT WORK **
            //driver.findElement(By.xpath("/html/body/app-root/div/div/div/app-user-preferences/div[1]/div[3]/div/div/div/form/div[7]/div/label")).click();
            eyes.checkWindow();


            //STEP 11
            //Save
            driver.findElement(By.xpath("/html/body/app-root/div/div/div/app-user-preferences/div[1]/div[2]/input")).click();
            //if the email account already exists - handle it
            eyes.checkWindow();
            try {
                driver.findElement(By.cssSelector("body > app-root > div > div > div > app-user-preferences > div.modal.fade.in.show > div > div > div.modal-header > button")).click();
            } catch (Exception e) {
                //ok don't worry
            }

            //STEP 12
            Thread.sleep(5000);
            driver.findElement(By.xpath("//div[text()='Home']")).click();

            //STEP 13
            eyes.checkWindow();
            driver.findElement(By.xpath("/html/body/app-root/div/div/div/app-dashboard/app-widgets-grid/div/div/div[1]/app-calendar-widget/app-base-widget/article/div[1]/div/app-calendar/div/div/div[2]/div[31]")).click();

            //STEP 14
            eyes.checkElement(By.xpath("/html/body/app-root/div/div/div/app-dashboard/app-widgets-grid/div/div/div[1]/app-calendar-widget/app-base-widget/article"));

            //STEP15
            //how to interact with map via automation?

            //STEP 16
            //how to interact with map via automation?

            //STEP 17
            driver.findElement(By.xpath("//div[text()='Inspect.']")).click();
            eyes.checkWindow();

            //STEP 18
            Thread.sleep(10000);
            driver.findElement(By.xpath("/html/body/app-root/div/div/div/app-inspections/div[1]/kendo-grid/div/table/thead/tr/th[3]/kendo-grid-filter-menu/a")).click();
            eyes.checkWindow();

            //STEP 19
            driver.findElement(By.xpath("/html/body/app-root/kendo-popup/div/kendo-grid-filter-menu-container/form/div/kendo-grid-string-filter-menu/kendo-grid-string-filter-menu-input/kendo-grid-filter-menu-input-wrapper/input")).sendKeys("1997");
            driver.findElement(By.xpath("/html/body/app-root/kendo-popup/div/kendo-grid-filter-menu-container/form/div/div/button[2]")).click();
            */

			//STEP 20

			Thread.sleep(5000);
			System.out.println("final checkWindow");
			eyes.checkWindow();
		}
	}
