package stepdefs;

import com.applitools.eyes.visualgrid.model.TestResultSummary;
import com.zfs.utility.EyesInit;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java.en.And;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.*;
import org.openqa.selenium.firefox.*;

import com.applitools.eyes.MatchLevel;
import com.applitools.eyes.RectangleSize;
import com.applitools.eyes.selenium.Eyes;
import com.applitools.eyes.visualgrid.services.VisualGridRunner;

public class KineticDSteps {
	
	WebDriver driver;
	
	// Initialize the eyes SDK and set your private API key.
	// Create a runner with concurrency of 10
    VisualGridRunner runner = new VisualGridRunner(10);

    //Initialize Eyes with Visual Grid Runner
    String AppName = "ZFS-MotionKinetic";
    String TestName = "ZFS-MotionKinetic-Test";
    String BatchName = "ZFS-MotionKinetic-Test-Batch";
    Eyes eyes = EyesInit.initializeEyes(BatchName, AppName, TestName, runner);
    //Eyes eyes = new Eyes();

	@Then("^Close the Browser")
	public void close_the_browser() throws Throwable {
		Thread.sleep(5000);
		try {
			//eyes.close();
			TestResultSummary allResults = runner.getAllTestResults();
		} catch (Exception e){

        	e.printStackTrace();
        	
            // If the test was aborted before eyes.close was called, ends the test as aborted.
            eyes.abortIfNotClosed();

        } finally {

            // Close the browser.
            driver.quit();

        }

	}
	
	@Then("^Click on \"(.*?)\" button in the \"(.*?)\"$")
	public void Click_on_button_in_the(String button_name, String page) throws Throwable {
		eyes.checkWindow();
		button(button_name,page);
		Thread.sleep(3000);
		driver.manage().timeouts().implicitlyWait(14, TimeUnit.SECONDS);
	}
	 
	@Then("^Enter \"([^\"]*)\" as \"([^\"]*)\" in the \"*([^\"]*)\"$")
	public void Enter_as_in_the1(String text_field, String name, String page) throws Throwable {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		if (name.contains("xxxxx")) {
			name = "zur1ch18";
		}
		textbox(text_field,name,page);
	}
	
	@Then("^Enter the \"([^\"]*)\" as \"([^\"]*)\" in the \"*([^\"]*)\"$")
	public void enter_the_as_in_the(String arg1, String arg2, String arg3) throws Throwable {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		WebElement el3 = driver.findElement(By.xpath("//input[@name='j_username']"));
		el3.clear();
		el3.sendKeys("NM");
	}
	
	@Then("^Enter username value as \"(.*?)\" in the \"(.*?)\"$")
	public void enter_username_value_as_in(String arg1, String arg2) throws Throwable {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		WebElement el4 = driver.findElement(By.xpath("//input[@name='Username']"));
		el4.clear();
		el4.sendKeys("NM");
	}
	
	@Given("^Open the \"([^\"]*)\" browser in laptop and launch the application$")
	public void open_the_browser_in_laptop_and_launch_the_application(String browser) throws Throwable {
		
		eyes.setApiKey(System.getenv("APPLITOOLS_API_KEY"));
		//eyes.setApiKey("yourkey");

        eyes.setMatchTimeout(10000);
        eyes.setMatchLevel(MatchLevel.STRICT);

		if (browser.contains("firefox")) {
			driver = new FirefoxDriver();
		} else {
			//System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
			driver = new ChromeDriver();
		}
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("http://zurichtest.motionkinetic.net/");
		
		//********************** E N T E R    P R O X Y   I N F O   H E R E ***************************
		//if you need to specify internet proxy info do it here
		//eyes.setProxy(new ProxySettings("http://YOUR-PROXY-URI"));
		//OR
		//eyes.setProxy(new ProxySettings("http://YOUR-PROXY-URI", YOUR_USER, YOUR_PASSWORD));
		//*********************************************************************************************
		
		
		eyes.open(driver, AppName, TestName,
                new RectangleSize(1024, 768));
		
	}
	
	@When("^Click on \"(.*?)\" button from \"(.*?)\"$")
	public void click_on_the_button_from(String button_name, String page) throws Throwable
	{
		eyes.checkWindow();
		button_symbol(button_name, page);
		//AW - wasn't sure how to implement the following line
		//sync();
	}
	
	@Then("^Click on \"(.*?)\" link from \"(.*?)\"$")
	public void click_on_link_from(String link_name, String page) throws Throwable {
		eyes.checkWindow();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		link(link_name, page);
		Thread.sleep(2000);
	}
	
	@Then("^Enter the value for text box \"([^\"]*)\" editbox as \"([^\"]*)\" in the \"*([^\"]*)\"$")
	public void enter_the_value_for_text_box_editbox__as_in_the(String field_name, String value, String page) throws Throwable {
		eyes.checkWindow();
		customeredit(field_name,value,page);
	}
	
	public void button(String button_name, String page) throws Throwable {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		WebElement submitbutton;
		String queryXpath = null;
		
		if (button_name.contains("Save")) {
			submitbutton = driver.findElement(By.xpath("//input[@value='Save']"));
		} else {
			//submitbutton = driver.findElement(By.xpath("//input[@value='submit']"));
			System.err.println("button name: " + button_name);
			if (button_name.contains("Log")) {
			queryXpath = "//button[contains(text(),'" + button_name + "')]";
			}
			if (button_name.contains("Create")) {
			queryXpath = "//a[contains(text(),'" + button_name + "')]";
				}
				
			submitbutton = driver.findElement(By.xpath(queryXpath));
			button_name = "";
		}
		submitbutton.click();
		Thread.sleep(3000);
		
		//AW - wasn't sure how to implement the following line
		//sync();
	}
	
	public void button_symbol(String button_name, String page) throws Throwable {
		System.err.println("button name: " + button_name);
		String queryXpath = null;
		if (button_name.contains("Log")) {
		queryXpath = "//button[contains(text(),'" + button_name + "')]";
		}
		if (button_name.contains("Create")) {
		queryXpath = "//a[contains(text(),'" + button_name + "')]";
			}
		System.err.println("query xpath = " + queryXpath);
		driver.findElement(By.xpath(queryXpath)).click();
		
		Thread.sleep(3000);
	}
	
	public void textbox(String text_field, String name, String page) throws Throwable {
		WebElement el1 = driver.findElement(By.xpath("//input[@name='"+text_field+"']"));
		el1.clear();
		el1.sendKeys(name);
	}
	
	public void link(String link_name, String page) throws Throwable {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//span[contains(text(), '"+link_name+"')]")).click();
	}
	
	public void sublink(String sublink_name, String page) throws Throwable {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//span[contains(text(), '"+sublink_name+"')]")).click();
	}
	
	public void customeredit(String field_name, String value, String page) throws Throwable {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		WebElement el2 = driver.findElement(By.xpath("//*[contains(text(),'"+field_name+"')]/following::input[contains(@class, 'form-control')]"));
		el2.isDisplayed();
		el2.sendKeys(value);
	}
	
	@And("^Set Customer Details")
	public void set_Customer_Details() throws Throwable {
		//Managed case switch
		driver.findElement(By.xpath("//*[@id=\"sectCustPolicyNos_Div\"]/div[2]/div[1]/div[3]/div/div/label[3]")).click();
		
		//CSI Switch
		driver.findElement(By.xpath("//*[@id=\"sectCustPolicyNos_Div\"]/div[2]/div[1]/div[8]/div/div/label[3]")).click();
		
		eyes.checkWindow();	
	}
	
	@And("^Logout from the application")
	public void logout_from_the_application() throws Throwable {
		driver.findElement(By.xpath("//*[@id=\"page-wrapper\"]/nav/ul/li[3]/a/i")).click();
		eyes.checkWindow();	
	}
	
}
